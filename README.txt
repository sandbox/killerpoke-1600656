

Domain Toolkit
======================================================================
Domain Access is used to create multiple domains and grant user access 
to this domains. For the D7-Version of Domain Access, there are some 
contributed modules missing (e.g. hierarchical domains, inherited 
permissions, �). This features are implemented by custom modules, 
bundled in domain_toolkit.



Relationships
======================================================================

Domains
----------------------------------------------------------------------
Every domain has a parent (or -1 for the root element) domain. This 
relationship is saved in the domain table for every domain. 

Users
----------------------------------------------------------------------
Every user is assigned to one or more Domains. This assignment stands 
for the ability to manage the structure of elements (menus, �) of this 
domain, creating content FROM this domain and publishing content TO this 
domain and it's children. 

Content
----------------------------------------------------------------------
Every node has one source domain and one or more publish domain(s). The 
source domain is the domain, the node was published from. Editing & 
deleting this node can only be done from the source domain (**ATTENTION**: 
This differs greatly from the original Domain Access behavior!). Because 
of this, it's possible to publish nodes to child domains, without 
providing them with the ability to edit or delete the node. 



Menus
======================================================================
The module "domain_menu_block" provides the ability to create menus for 
each domain. Drupal only allows to assign the ability to manage all or 
none to each user. To allow the user to only manage menus assigned to 
his/her domains, we change the default Drupal behavior. (**ATTENTION**: 
There is a new ability to "Only manage menus, assigned to one of my 
domains." which needs to be assigned to all site-builders. )

Usage
----------------------------------------------------------------------
Every Domain have it's own menu. According to the subdomain, the 
domain-menu-block displays the right menu.

Menus & content-types
----------------------------------------------------------------------
By default you would need to assign all possible menus to each content-
type. When there are several hundred domains in your system this 
procedure is not really posible. In the content-type menu-settings there
will only be one generic domain for all instances (for each domain). 
Superadmins can assign all menus of the same type with one click. 
(**ATTENTION**: Since the menu - content-type relationship can not 
be altered, all relationships are created on content-type-save. When 
a new domain is created, you need to save all content types again.)



Views
======================================================================
To display content according to domains on pages like frontage or 
calendar there are three new view filter to filter content: 

Filter for current domain, including children.
----------------------------------------------------------------------
This filter is in behavior to the source domain. The "publish to" 
settings of nodes will not affect this filter.

Filter the domain_id by domains assigned to current user.
----------------------------------------------------------------------
To allow the admin-user to see all nodes, published to his/her domains, 
this filter is used.

Filter the source domain of the node by domains assigned to current user.
----------------------------------------------------------------------
Only display nodes, published FROM domains of the current user.