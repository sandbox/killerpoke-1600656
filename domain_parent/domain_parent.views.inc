<?php

/**
 * @file
 * Provides the views data and handlers for domain_parent.module.
 *
 * @ingroup domain_parent
 */

/**
 * Implements hook_views_data_alter().
 */
function domain_parent_views_data_alter(&$data) {
  
  // If the domain_views module is installed.
  if(isset($data["domain"])) {
  	
  	// Add filter for parent
  	$data['domain']['parent'] = array(
	    'title' => t('Domain parent'),
	    'help' => t('Filter for current domain, including children.'),
	    'real field' => 'parent',
	    'field' => array(
	      'click sortable' => TRUE,
	    ),
	    // Information for accepting a domain_id as a filter
	    'filter' => array(
	    	'field' => 'domain_id', 
	      'handler' => 'domain_parent_handler_filter_domain_inherited_access',
	    ),
	  );
	  
	  // Add filter for parent
  	$data['domain']['user_domain_id'] = array(
	    'title' => t('User Domains'),
	    'help' => t('Filter the domain_id by domains assigned to current user.'),
	    'real field' => 'parent',
	    'field' => array(
	      'click sortable' => TRUE,
	    ),
	    // Information for accepting a domain_id as a filter
	    'filter' => array(
	    	'field' => 'domain_id', 
	      'handler' => 'domain_parent_handler_filter_domain_access',
	    ),
	  );
	  
	  // Define the fields.
	  $data['domain_source']['user_source_domain'] = array(
      'title' => t('User Domains'),
      'help' => t('Filter the source domain of the node by domains assigned to current user.'),

      // Information for accepting a domain_id as a filter
      'filter' => array(
	      'field' => 'domain_id', 
        'handler' => 'domain_parent_handler_filter_domain_inherited_domain_id',
      ),
    );
  }
}

