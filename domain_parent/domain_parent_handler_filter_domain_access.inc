<?php

/**
 * @file
 * Interface between domain_parent.module and views.module.
 *
 * @ingroup domain_parent
 */

/**
 * Filter out nodes published to current domain or children
 */
class domain_parent_handler_filter_domain_access extends views_handler_filter_boolean_operator {
  function query() {
        
  	if($this->value) {
	  	// Get all children for current domain
  		/* The plan: We collect down all domains from the current one, 
  		   so we can add a simple query "...where domain is in(...)". 
  		   the collection can be cached, so we dont need to collect
  		   the domains on every request.
  		*/
  		global $user;
  		
  		$domains = array();
  		foreach($user->domain_user as $domain) {
  			array_push($domains, $domain["domain_id"]);
  		}


	    $table = $this->ensure_my_table();
	    $this->query->add_where($this->options['group'], "$table.$this->real_field", $domains);
	  }
  }
}
