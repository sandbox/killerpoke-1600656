<?php

/**
 * @file
 * Interface between domain_parent.module and views.module.
 *
 * @ingroup domain_parent
 */

/**
 * Filter out nodes published to current domain or children
 */
class domain_parent_handler_filter_domain_inherited_access extends views_handler_filter_boolean_operator {
  function query() {
        
  	if($this->value) {
	  	// Get all children for current domain
  		/* The plan: We collect down all domains from the current one, 
  		   so we can add a simple query "...where domain is in(...)". 
  		   the collection can be cached, so we dont need to collect
  		   the domains on every request.
  		*/
  		$domains = array($this->value);
  		$children = array();
  		domain_parent_flatten_tree(domain_parent_children_for_parent($this->value), $children);

  		foreach($children as $child) {
  			array_push($domains, $child["domain_id"]);
  		}


	    $table = $this->ensure_my_table();
	    $this->query->add_where($this->options['group'], "$table.$this->real_field", $domains);
	  }
  }
}
