<?php

/**
 * @file
 * Interface between domain_parent.module and views.module.
 *
 * @ingroup domain_views
 */

/**
 * Filter for domain ids stored in the database, currently active domain and any domain
 */
class domain_parent_handler_filter_domain_inherited_domain_id extends views_handler_filter_boolean_operator  {
  
  function query() {
    
    if($this->value == 1) {
      global $user;
      $domains = array();
      
  		foreach ($user->domain_user as $domain) {
  			array_push($domains, $domain);
  		}
  
      $table = $this->ensure_my_table();
      $this->query->add_where($this->options['group'], "$table.$this->real_field", $domains);
    }
  }
}
